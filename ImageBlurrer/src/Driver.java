import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

//Driver class which takes in appropriate image information input and then initializes the image class for
//manipulation to be blurred
public class Driver {
	
	public static void main(String[] args) throws Exception {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("\nPlease enter the full file path for the image:");
		String filePathName = scanner.nextLine();
		System.out.print("\nPlease enter the width of the image: ");
		int width = scanner.nextInt();
		System.out.print("Please enter the height of the image: ");
		int height = scanner.nextInt();
		
		System.out.print("The file is: " + width + "x" + height + ")... ");
		RawGray rg = new RawGray(filePathName, width, height);
		if(rg.greyScaleCheck){
			System.out.println("Greyscale image loaded properly!");
		}
		else {
			System.out.println("Color image loaded properly!");
		}
		
		//for some reason have to refresh the scanner to continue program
		boolean scannerCheck = true;
		while (scannerCheck) {
			try {
				System.out.print("\n Do you want to continue? If so, type anything and press enter ");
				String test = scanner.nextLine();
				if (test.isEmpty()) throw new Exception("");
				scannerCheck = false;
			} catch (Exception e) { 
				System.err.println(e.getMessage()); 
				}
		}
		rg.blur();
		System.out.println("This image was successfully blurred !");
		
		System.out.println("\nPlease enter the full file path for the output image, including .raw extension:");
		String outputPath = scanner.nextLine();
		
		System.out.print("We are now writing contents to file. ");
		rg.save(outputPath);
		System.out.println("Our image was image successfully saved!");
		
		scanner.close();
	}
}

//abstraction for image file; this is responsible for blurring the image, saving it, and has a subclass of a 
//pixel object which deals with individual pixel values

class RawGray {
	public final boolean greyScaleCheck;
	private final int width,height;
	private PixelObject[][] pixelArray;
	
	//inner class pixel object abstraction that essentially deals with individual pixel issues like color
	private static class PixelObject implements Cloneable {
		//set red, green, and blue channels
		
		private byte r,g,b;
		
		public PixelObject(byte[] rgb) {
			if (rgb.length == 3) {
				this.r = rgb[0];
				this.g = rgb[1];
				this.b = rgb[2];
			} 
			else {
				//else it is grayscale so set to one channel
				this.r = rgb[0];
				this.g = 0;
				this.b = 0;				
			}
		}
		public PixelObject(byte r, byte g, byte b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}
		public void obtainBytes(byte[] bytes) {
			if (bytes.length == 3) { 
				bytes[0] = r;
				bytes[1] = g;
				bytes[2] = b;
			} else { 
				bytes[0] = r;
			}
		}
		public static PixelObject doAverage(PixelObject[] pixelObject) {
			
			if (pixelObject.length == 0) {
				return null;
			}
			int r = 0,g = 0,b = 0;
			for (PixelObject pixel : pixelObject) {
				r += pixel.r & 0xFF;
				g += pixel.g & 0xFF;
				b += pixel.b & 0xFF;
			}
		
			r = Math.round(r/(float)pixelObject.length);
			g = Math.round(g/(float)pixelObject.length);
			b = Math.round(b/(float)pixelObject.length);

			return new PixelObject((byte)r,(byte)g,(byte)b);
		}
		
		//necessary clone method override 
		protected Object clone() { 
			return new PixelObject(r,g,b);
			}
	}
	
	//constructor for RawGray file
	public RawGray(String filePath, int width, int height) throws Exception {
		File file = new File(filePath);
		//ensures the file exists
		if (!file.exists()) throw new FileNotFoundException(filePath); 
		
		//set the reference width and height to this specific object's instance
		this.width = width;
		this.height = height;
		pixelArray = new PixelObject[height][width];
		
		int greyScaleSize = width*height;
		//checks if the image is grayscale
		if (greyScaleSize == file.length()) {
			greyScaleCheck = true; 
		}
		//checks if image is a color
		else if (greyScaleSize*3 == file.length()){
			greyScaleCheck = false; //is color
		}
		//else exits the program because an incorrect image format was given
		else throw new Exception("You didn't input a raw, no header grayscale or color file."); 
		
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		
		byte[] bytes;
		if(greyScaleCheck){
			 bytes = new byte[1];
		}
		else {
			 bytes = new byte[3];
		}
		
		//this reads the files bytes into our 2D pixal array
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				raf.read(bytes);
				pixelArray[i][j] = new PixelObject(bytes);
			}
		}
		raf.close();
	}
	
	//method to save the newly created image, have to make a custom method due to scanner issues
	public void save(String filePath) throws IOException {
		File file = new File(filePath);
		file.delete(); 
		file.createNewFile(); 
		
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		
		byte[] bytes;
		if(greyScaleCheck){
			bytes = new byte[1];
		}
		else {
			bytes = new byte[3];
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				pixelArray[i][j].obtainBytes(bytes);
				raf.write(bytes);
			}
		}
		raf.close();
	}
	
	//method responsible for blurring image at default bit depth of 1
	public void blur() {
		
		PixelObject[][] pixelArray2 = new PixelObject[height][width];
		
		//clones our pixel array
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				pixelArray2[i][j] = (PixelObject)pixelArray[i][j].clone();
			}
		}
		//this is averaging the pixels based on the bit depth default of 1
		for (int i = 0; i < height; i++) { 
			for (int j = 0; j < width; j++) { 
				
				ArrayList<PixelObject> pixelList = new ArrayList<PixelObject>(); 
				for (int k = i-1; k <= i+1; k++) { 
					for (int l = j-1; l <= j+1; l++) { 
						
						//checking for side and outside layer pixels (will break)
						if (k < 0 || k >= height) break; 
						if (!(l < 0 || l >= width)) { 
							pixelList.add(pixelArray2[k][l]); 
						}
					}
				}
				
				//puts the new averaged value in the appropriate place
				pixelArray[i][j] = PixelObject.doAverage(pixelList.toArray(new PixelObject[0])); 
			}
		}
	}
}